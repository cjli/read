---
title: composition
---

Go supports composition, which is the act of including one structure into another. In some languages, this is called a trait or a mixin.

Is composition better than inheritance? Many people think that it’s a more robust way to share code. When using inheritance, your class is tightly coupled to your superclass and you end up focusing on hierarchy rather than behavior.