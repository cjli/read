---
title: Pointer
---

Many times though, we don’t want a variable that is directly associated with our value but rather a variable that has a pointer to our value.

A pointer is a memory address; it’s the location of where to find the actual value. It’s a level of indirection.

Why do we want a pointer to the value, rather than the actual value? It comes down to the way Go passes arguments to a function: as copies.

`*X` means pointer to value of type X. There’s obviously some relation between the types `X` and `*X`, but they are two distinct types.

The real value(价值) of pointers though is that they let you share values.