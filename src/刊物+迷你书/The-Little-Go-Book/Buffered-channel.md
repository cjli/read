---
title: Buffered Channel
---

Buffered channels don’t add more capacity; they merely provide a queue for pending work and a good way to deal with a sudden spike.

A main purpose of select is to manage multiple channels. Given multiple channels, select will block until the first one becomes available. If no channel is available, default is executed if one is provided. A channel is randomly picked when multiple are available.