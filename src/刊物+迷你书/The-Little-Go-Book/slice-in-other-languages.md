---
title: slice in other languages
---

If the underlying array is full, it will create a new larger array and copy the values over (this is exactly how dynamic arrays work in PHP, Python, Ruby, JavaScript, …).

However, in these languages, a slice is actually a new array with the values of the original copied over.