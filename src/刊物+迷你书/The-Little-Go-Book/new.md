---
title: new
---

Structures don’t have constructors.

Despite the lack of constructors, Go does have a built-in new function which is used to allocate the memory required by a type.