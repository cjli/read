---
title: Variable declaring
---

It’s important that you remember that `:=` is used to declare the variable as well as assign a value to it. Why? Because a variable can’t be declared twice (not in the same scope anyway).

Remember that you’ll use `var NAME TYPE` when declaring a variable to its zero value, `NAME := VALUE` when declaring and assigning a value, and `NAME = VALUE` when assigning to a previously declared variable.

If you’re coming from a dynamic language, the complexity around types and declarations might seem like a step backwards. I don’t disagree with you. For some systems, dynamic languages are categorically more productive.