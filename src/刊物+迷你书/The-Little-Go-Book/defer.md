---
title: defer
---

As we’re writing a function, it’s easy to forget to Close something that we declared 10 lines up.

For another, a function might have multiple return points. Go’s solution is the defer keyword.