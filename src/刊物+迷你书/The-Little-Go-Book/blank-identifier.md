---
title: Blank identifier
---

`_`, the blank identifier, is special in that the return value isn’t actually assigned. This lets you use `_` over and over again regardless of the returned type.