---
title: What go suit for
---

Maybe it’s a messaging, caching, computational-heavy data analysis, command line interface, logging or monitoring. 

Go is becoming increasingly popular as a language for command-line interface programs and other types of utility programs you need to distribute (e.g., a log collector).

You can use Go to build websites (and many people do), but I still prefer, by a wide margin, the expressiveness of Node or Ruby for such systems.