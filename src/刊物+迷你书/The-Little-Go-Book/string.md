---
title: String
---

Do note that when you use `[]byte(X)` or `string(X)`, you’re creating a copy of the data. This is necessary because strings are immutable.

Strings are made of runes which are unicode code points.