---
title: Interface
---

Interfaces are types that define a contract but not an implementation.

Since every type implements all 0 of the empty interface’s methods, and since interfaces are implicitly implemented, every type fulfills the contract of the empty interface.

To convert an interface variable to an explicit type, you use `.(TYPE)`: `i := f1(); j, err := i.(int)`

Converting values back and forth is ugly and dangerous but sometimes, in a static language, it’s the only choice.