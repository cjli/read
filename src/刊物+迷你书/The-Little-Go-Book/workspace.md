---
title: Workspace
---

Go is designed to work when your code is inside a workspace. The workspace is a folder composed of bin, pkg and src subfolders. You might be tempted to force Go to follow your own style-don’t.

`go get` fetches the remote files and stores them in your workspace.