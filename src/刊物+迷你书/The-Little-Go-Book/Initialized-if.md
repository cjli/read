---
title: Initialized if
---

Go supports a slightly modified if-statement, one where a value can be initiated prior to the condition being evaluated:

``` go
if err := process(); err != nil {
      return err
}
```

Interestingly, while the values aren’t available outside the if-statement, they are available inside any else if or else.