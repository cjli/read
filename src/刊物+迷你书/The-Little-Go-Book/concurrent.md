---
title: Concurrent 
origin: origin
---

Writing concurrent code requires that you pay specific attention to where and how you read and write values. In some ways, it’s like programming without a garbage collector–it requires that you think about your data from a new angle, always watchful for possible danger.

The only concurrent thing you can safely do to a variable is to read from it. You can have as many readers as you want, but writes need to be synchronized. There are various ways to do this, including using some truly atomic operations that rely on special CPU instructions. However, the most common approach is to use a mutex:

The challenge with concurrent programming stems from sharing data.