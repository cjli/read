---
title: Package import
---

In Go, the import keyword is used to declare the packages that are used by the code in the file.

Go is strict about importing packages. It will not compile if you import a package but don’t use it. Go is strict about importing packages. It will not compile if you import a package but don’t use it.

The important rule about shared packages is that they shouldn’t import anything from any sub-packages. (Avoid cyclic importing)