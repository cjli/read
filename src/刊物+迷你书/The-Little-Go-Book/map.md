---
title: map
---

Maps in Go are what other languages call hashtables or dictionaries. They work as you expect: you define a key and value, and can get, set and delete values from it.

Iteration over maps isn’t ordered. Each iteration over a lookup will return the key value pair in a random order.