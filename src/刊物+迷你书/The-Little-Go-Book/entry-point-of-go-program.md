---
title: Entry point of go program 
---

In Go, the entry point to a program has to be a function called main within a package main.

Try making changes of main package name or main function name to something else, but use go build instead. Notice that the code compiles, there’s just no entry point to run it. This is perfectly normal when you are, for example, building a library.