---
title: 谁更适合编程入门语言：C or Python？
---

一、C

C 标准委员会写的 C 语言基本原理中谈到 C 的设计精神，有一句话：

> Make it fast, even if it is not guaranteed to be portable.

这句话奠定了 C 语言的设计首先要适应机器直觉，其次才是适应人的直觉。所以才会有指针，这种活生生的内存地址展示；数组下标从 0 而不是从 1 开始等反人类的设计。

因此，想驾驭 C，就必须了解机器的秉性。

二、Python

Python 的设计理念是无所不用其极地适应人的直觉，所以获得了「可执行的伪代码」美誉。

这样带来一个很大的好处，就是 Python 学习者的注意力无需过多纠缠于语法等细节，而是可以更多地集中在「程序设计思维」，这是初学编程最重要的东西。

三、总结

1. 立志做技术行，从 C 语言入门百利无害。

2. 想不清楚是否做技术行，两个语言皆可，看缘分了。

3. 不做技术，必须 Python。

