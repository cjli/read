---
title: Java 能做什么？
---

1. 大数据分析

Java 拥有完整的大数据技术体系，包括但不限于 Hadoop，hbase，spark，storm 用来处理海量数据。

2. 分布式集群

Java 有大量功能完善的分布式服务中间件，避免重新开发此类服务，比如：zookeeper、Kafka，hdfs 等。

3. 搜索引擎

搜索引擎目前也是 Java 一家独霸，Java 的 Elastic Search 是目前最好的开源搜索引擎。此外围绕 elastic search 的 ELK 日志分析工具，也已经形成了生态链，发挥着越来越多的用途。

总结：Python 更适合用在创业开发或者对业务变化需求非常高的公司，而 Java 更适合对业务要求稳定，并且有海量数据需要处理的公司。
