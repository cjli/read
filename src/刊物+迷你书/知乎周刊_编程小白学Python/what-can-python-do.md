---
title: Python 能做什么
---

Python 强项在于，其他语言几百句做的事情 Python 可以几十行，帮助开发者专注问题。

> Python 的哲学：用一种方法，最好是只有一种方法来做一件事。

Python 的缺点就是慢，只能优化算法，要常数优化只能换类似 Java 的语言。

总结来说，Python 的应用领域有：

1. 网站业务逻辑开发

django——支持各种主流数据库，有好用的 ORM 系统和模版系统，完善的第三方库能帮助解决遇到的大部分问题。

2. 数据分析和科学计算

NumPy，SciPy 库科学计算库；Pandas 数据分析库；Matplotlib 绘图库。

3. 网络爬虫

Scrapy 爬虫库（结合 rq-queue 很容易构造一个分布式爬虫）、beatifulsoup，pyquery 等 HTML 解析库、requests 网络库。

4. 自动化运维

saltstack、ansible。

5. 游戏

6. 命令行实用工具
