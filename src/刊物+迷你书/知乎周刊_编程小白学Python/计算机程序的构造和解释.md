---
title: 编程学习必读书籍——《计算机程序的构造和解释》
origin: 编程入门指南
---

设想：

X = 用于思考解决方案的时间，即「解决问题」部分

Y = 用于实现代码的时间，即「利用计算机」部分

编程能力 = F(X, Y) (X > Y)

要想提高编程能力，就得优化 X，Y 与函数 F(X, Y)，很少有书的内容能同时着重集中在这三点上，SICP 除外。SICP 为你指明了这三个变量的方向。

在阅读 SICP 之前，你也许能通过调用几个函数解决一个简单问题，但阅读完 SICP 之后，你会学会如何将问题抽象并且分解，从而处理更复杂更庞大的问题，这是编程能力巨大的飞跃，这会在本质上改变你思考问题以及用代码解决问题的方式。

Peter Norvig 增加写过一段非常精彩的 SICP 书评（Amazon.com）：

> To use an analogy, if SICP were about antomobiles, it would be for the person who wants to know how cars work, how the are built, and how one might design fuel-efficient, safe, reliable vehicles for the 21st century.

> The people who hate SICP are the ones who just want to know how to drive their car on the highway, just like everyone else.
