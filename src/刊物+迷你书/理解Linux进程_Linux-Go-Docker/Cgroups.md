---
title: Cgroups
---

Cgroups全称ControlGroups，是Linux内核用于资源隔离的技术。目前Cgroups可以控制CPU、内存、磁盘访问。