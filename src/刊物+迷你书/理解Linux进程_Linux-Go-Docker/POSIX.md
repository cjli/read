---
title: POSIX
---

POSIX(Portable Operation SystemInterface)听起来好高端，就是一种操作系统的接口标准。有了这个规范，你就可以调用通用的API了，Linux提供的POSIX系统调用在Unix上也能执行，因此学习Linux的底层接口最好就是理解POSIX标准。