---
title: 为什么 `gun run` 每次的 ppid 都不一样
---

`go run` 每次都会启动一个新的 Go 虚拟机来执行进程。因此 ppid 不一样。