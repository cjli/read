---
title: select poll epoll
---

Poll 本质上是 Linux 系统调用，其接口为 `int poll(struct pollfd *fds,nfds_t nfds, inttimeout)`，作用是监控资源是否可用。

epoll 不存在这个问题是因为它仅关注活跃的 socket 。

无论是select、poll还是epoll，他们都要把文件描述符的消息送到用户空间，这就存在内核空间和用户空间的内存拷贝。其中 epoll 使用 mmap 来共享内存，提高效率。