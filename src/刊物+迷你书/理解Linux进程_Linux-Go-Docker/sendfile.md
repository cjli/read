---
title: Sendfile 
---

Sendfile是Linux实现的系统调用，可以通过避免文件在内核态和用户态的拷贝来优化文件传输的效率。