---
title: 投资的方式
---

一方面，投资是指通过获取利息使钱的数目增加，比如将钱投资买股票，或者存入银行；另一方面，通过购买任何一件日后会增值的物品，比如艺术品、房产、古董等，也可以进行投资。