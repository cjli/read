---
title: COBIT
---

COBIT（Control Objectives for Information and RelatedTechnology，信息和相关技术的控制目标）给出了一个很好的定义：

治理通过评估干系人的需求、当前情况及下一步的可能性来确保企业目标的达成，通过排优先级和做决策来设定方向。对于已经达成一致的方向和目标进行监督。