---
title: 标点符号的作用
origin: 序
---

冒号善解释，引号善引用，问号善提问，逗号善缓冲，叹号善感叹，句号善总结。