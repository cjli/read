---
title: 合成与继承
---

规范的合理度和遵循度很大程度决定了软件的质量。DIP与‘针对接口编程’原则一脉相承，提倡编程应依赖规范而非实现、依赖抽象而非细节。它还顺便印证了我们从前的观点——具体类不宜被继承，因为继承是一种深度依赖，而具体类不足够抽象。”

合成关系与继承关系都是一种依赖。合成比继承的一个优势是它是隐性的、动态的，这使得在合成类与被合成类之间有了插入抽象层的可能。当抽象层利用了多态机制时，这种合成也被称为多态合成（polymorphic composition）。