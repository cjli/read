---
title: 程序员的四层境界
origin: 第一课：开班导言
---

学会不如会学，会学不如会用，会用不如被用。对于一个软件开发者来说，这意味着 4 个阶段：

1. 学会（知其所然）——掌握一些具体编程知识的初级程序员。

2. 会学（知所以然）——能快速而深刻地理解技术并举一反三的程序员。

3. 会用（人为我用）——能将所学灵活运用到实际编程设计之中的高级程序员。

4. 被用（我为人用）——能设计出广为人用的应用程序（application）、库（library）、工具包（toolkit）、框架（framework）等的系统分析师和架构师。

至于被用的更高层次，如发明出主流的设计模式、算法、语言，乃至理论等，则可称得上计算机专家了。