---
title: 程序员是吃青春饭的职业吗
---

开放言论才能解放思想，思想解放了才能产生灵感和激情。缺乏灵感和激情的程序员，学习起来吃力，工作起来辛苦，最后就会感慨这是吃青春饭的职业。