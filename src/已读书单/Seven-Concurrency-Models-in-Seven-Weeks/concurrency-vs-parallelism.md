---
title: Concurrency vs Parallelism
origin: Introduction
---

Concurrency is an aspect of the problem domain - your program needs to handle multiple simultaneous(or near-simulataneous) events.

A concurrent program has multiple logical threads of control. These threads may or may not run in parallel.

Parallelism, by contrast, is an aspect of the solution domain - you wnat to make your program faster by processing different portions of the problem in parallel.

A parallel program potentially runs more quickly than a sequential program by executing different parts of the computation simultaneously(in parallel). It may or may not have more than one logical thread of control.

As Rob Pike puts it:

- Concurrency is about dealing with lots of things at once.

- Parallelism is about doing lots of things at once.

What parallelism and concurrency have in common is that they both go beyond the traditional sequential programming model in which things happen one at a time, one after the other.
