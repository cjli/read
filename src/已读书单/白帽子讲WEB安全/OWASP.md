---
title: OWASP
origin: https://www.owasp.org
---

开放式Web应用程序安全项目（OWASP）是一个在线社区，在Web应用程序安全性领域提供免费的文章，方法，文档，工具和技术。