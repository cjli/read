---
title: Session Fixation
---

这个没有换“锁”而导致的安全问题，就是 Session Fixation 问題。在用户登录网站的过程中，如果登录前后用户的 SessionID 没有发生变化，则会存在 Session Fixation 问题。

实际案例：discuz 7.2 wap 版本。

解决 Session Fixation 的正确做法是，在登录完成后，重写 SessionID。