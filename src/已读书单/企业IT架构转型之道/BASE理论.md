---
title: BASE 理论
---

BASE 理论是对 CAP 理论的延伸，核心思想是即使无法做到强一致性（ Strong Consistency， CAP 的一致性就是强一致性），但应用可以采用适合的方式达到最终一致性（ Eventual Consitency）。 

BASE 是指基本可用（ Basically Available）、柔性状态（ Soft State）、最终一致性（ Eventual Consistency）。 

- 基本可用

是指分布式系统在出现故障的时候，允许损失部分可用性，即保证核心可用。电商大促时，为了应对访问量激增，部分用户可能会被引导到降级页面，服务层也可能只提供降级服务。这就是损失部分可用性的体现。

- 柔性状态
 
是指允许系统存在中间状态，而该中间状态不会影响系统整体可用性。分布式存储中一般一份数据至少会有三个副本，允许不同节点间副本同步的延时就是柔性状态的体现。 MySQL Replication的异步复制也是一种柔性状态体现。

传统数据库的事务确实非常好地保证了业务的一致性，但在互联网场景下，比如上文提到淘宝订单和互联网金融 P2P 场景下，就暴露出数据库性能和处理能力上的瓶颈。所以在分布式领域，基于 CAP 理论和在其基础上延伸出的 BASE 理论，有人提出了“柔性事务”的概念。

- 最终一致性

是指系统中的所有数据副本经过一定时间后，最终能够达到一致的状态。弱一致性和强一致性相反，最终一致性是弱一致性的一种特殊情况。