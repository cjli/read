---
title: 比特币历史发展
---

1. 2008 年 11 月，当时一个化名中本聪的人在一次隐秘的密码学小组讨论中发表了一篇名为《比特币：一种点对点的电子现金系统》的论文，文中首次提出了比特币的概念。
2. 中本聪发表论文后开始着手开发比特币的发行、交易和账户管理系统。2009 年 11 月 3 日，该系统正式运行，区块链中的第一个区块诞生（创世区块）。
3. 2009 年 1 月 12 日，中本聪发送了 10 比特给密码学专家哈尔·芬尼。
4. 2010 年 5 月 21 日，佛罗里达程序员用 1 万比特币购买了价值 25 美元的披萨优惠券，随着这笔交易诞生了比特币第一个公允汇率。
5. 2010 年 7 月，比特币交易所 Mt.Gox 在日本成立，世界上第一个比特币交易平台的成立。标志着比特币的价值开始得到认可，之后出现缓慢增长的趋势。
6. 在大量投资者的参与下，比特币的价格在随后几年一路上涨，并一直处于剧烈波动中。
