---
title: 程序和 CTO 的区别在哪？
---

程序员基本上去解决一个定义好的问题，去实施一个定义好的方案。

决策者需要定义问题是什么，以及权衡最佳方案是什么。不管是决策技术架构还是决策商业策略，都是非常复杂的思维过程，需要综合和权衡大量的信息，这种能力就不是简单愣着头搞下去能练出来的了，很多时候需要抬起头来看，免得只见树木不见森林。
