---
title: INVEST 原则
---

INVEST 原则 [ 即独立的（ Independent ）、可协商的（ Negotiable ）、有价值的（ Valuable ）、可估计的（ Estimable ）、小的（ Small ）且可测试的（ Testable ） ] 的用户故事 [ddVMFH] 及考虑其验收条件。