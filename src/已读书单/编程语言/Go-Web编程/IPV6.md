---
title: IPv6
---

IPv6 的设计过程中除了一劳永逸地解决了地址短缺问题以外，还考虑了在 IPv4 中解决不好的其它问题，主要有端到端 IP 连接、服务质量（ QoS ）、安全性、多播、移动性、即插即用等。