---
title: ssh examples
origin: 第十一章、远程联机服务器 SSH/XDMCP/VNC/RDP
---

- 第一次登录远程主机时不询问记录远程主机的公钥（主要在程序脚本中使用）

``` shell
ssh -o StrictHostKeyChecking=no user@remote_host
```

- 不登录远程主机给远程主机关机

``` shell
ssh -f user@remote_host shutdown -h now
```

- 以指定的带宽上传文件

``` shell
# 限制上传带宽为 100 Kbyte/s
# dd if=/dev/zero of=/path/to/file bs=1M count=1
scp -l 800 /path/to/file user@remote_host:/path/to/save
```