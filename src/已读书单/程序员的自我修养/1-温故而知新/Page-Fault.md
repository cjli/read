---
title: Page Fault
---

当进程所需 VP 找不到对应的 PP 时（不在内存中），硬件会捕捉到这个消息，即所谓的页错误。然后操作系统接管进程，负责将所需VP从磁盘中读出来并且装入内存，然后将内存中的这 PP 与VP之间建立映射关系。