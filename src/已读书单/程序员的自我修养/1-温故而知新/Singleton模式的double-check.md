---
title: Singleton 模式的 double-check
---

```
volatile T* pInst = 0;

T* GetInstance() {
     if (pInst == NULL) {
           lock();
	   if (pInst == NULL) {
		 pInst = new T;
	   }
	   unlock();
     }

     return pInst;
}
```

抛开逻辑，这样的代码乍看是没有问题的，当函数返回时，PInst总是指向一个有效的对象。而lock和unlock防止了多线程竞争导致的麻烦。双重的if在这里另有妙用，可以让lock的调用开销降低到最小。

但是实际上这样的代码是有问题的。问题的来源仍然是CPU的乱序执行。C++里的new其实包含了两个步骤：（1）分配内存。（2）调用构造函数。

所以pInst = newT包含了三个步骤：

（1）分配内存。
（2）在内存的位置上调用构造函数。
（3）将内存的地址赋值给pInst。

在这三步中，（2）和（3）的顺序是可以颠倒的。也就是说，完全有可能出现这样的情况：pInst的值已经不是NULL，但对象仍然没有构造完毕。这时候如果出现另外一个对GetInstance的并发调用，此时第一个if内的表达式pInst==NULL为false，所以这个调用会直接返回尚未构造完全的对象的地址（pInst）以提供给用户使用。那么程序这个时候会不会崩溃就取决于这个类的设计如何了。