---
title: VP-PP-DP
---

VP/Virtual Page：进程内存的虚拟页
PP/Physic Page：物理内存的页
DP/Disk Page：磁盘中的页

有些 VP 会被映射到同一个 PP，从而实现内存共享。