---
title: 为什么从 2004 年后 CPU 频率的摩尔定律失效了?
---

在过去的 50 年里， CPU 的频率从几十 KHz 到现在的 4GHz ，整整提高了数十万倍，基本上每 18 个月频率就会翻倍。但是自 2004 年以来，这种规律似乎已经失效， CPU 的频率自从那时开始再也没有发生质的提高。原因是人们在制造 CPU 的工艺方面已经达到了物理极限，除非 CPU 制造工艺有本质的突破，否则 CPU 的频率将会一直被目前 4GHz 的“天花板”所限制。