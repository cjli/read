---
title: MMU
---

虚拟存储的实现需要依靠硬件的支持，对于不同的CPU来说是不同的。但是几乎所有的硬件都采用一个叫MMU（Memory ManagementUnit）的部件来进行页映射。

在页映射模式下，CPU 发出的是VirtualAddress，即我们的程序看到的是虚拟地址。经过MMU转换以后就变成了PhysicalAddress。一般MMU都集成在CPU内部了，不会以独立的部件存在。