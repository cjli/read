---
title: IO 密集与 CPU 密集 
---

我们一般把频繁等待的线程称之为IO密集型线程（IO BoundThread），而把很少等待的线程称为CPU密集型线程（CPU BoundThread）。IO密集型线程总是比CPU密集型线程容易得到优先级的提升。

