---
title: How to fix huge number of rows were examined
origin: c6-Slow Query Basics_Optimize Data Access
---

1. Use covering indexes, which store data so that the storage engine doesn’t have to retrieve the complete rows.

2. Change the schema. An example is using summary tables. (discussed in Chapter 4) 

3. Rewrite a complicated query so the MySQL optimizer is able to execute it optimally.
