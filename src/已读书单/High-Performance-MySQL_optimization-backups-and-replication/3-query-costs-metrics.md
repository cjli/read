---
title: Three query cost metrics
origin: c6-Slow Query Basics_Optimize Data Access
---

1. Response Time

- Service time: how long it takes the server to actually process the query.

- Queue time: the portion of response time during which the server isn’t really executing the query.

> it’s waiting for something, such as waiting for an I/O operation to complete, waiting for a row lock, and so forth.

Response time is not consistent under varying load conditions, factors like storage engine locks (table locks and row locks), high concurrency, and hardware can impact on it.

Nutshell: examine the query execution plan and the indexes involved, determine how many sequential and random I/O operations might be required, and multiply these by the time it takes your hardware to perform them.

2. Number of rows examined

It reveals how efficiently the queries are finding the data you need.

Howeveer, not all row accesses are equal. Shorter rows are faster to access, and fetching rows from memory is much faster than reading them from disk.

3. Number of rows returned

Ideally, the number of rows examined would be the same as the number returned, but in practice this is rarely possible.

Examples:

- when constructing rows with joins, the server must access multiple rows to generate each row in the result set.

- when using COUNT() aggregate function with GROUP BY clause, query returns only less rows, but it might needs to read thousands of rows to build the result set. (An index can’t reduce the number of rows examined for a query like this one.)