---
title: Why many queris are better than complex queries in MySQL
origin: c6-Ways to Restructure Queries 
---

The traditional approach to database design emphasizes doing as much work as possible with as few queries as possible. This approach was historically better because of the cost of network communication and the overhead of the query parsing and optimization stages.

However, this advice doesn’t apply as much to MySQL, because it was designed to handle connecting and disconnecting very efficiently and to respond to small and sim- ple queries very quickly. Modern networks are also significantly faster than they used to be, reducing network latency.

All else being equal, it’s still a good idea to use as few queries as possible, but sometimes you can make a query more efficient by decomposing it and executing a few simple queries instead of one complex one.