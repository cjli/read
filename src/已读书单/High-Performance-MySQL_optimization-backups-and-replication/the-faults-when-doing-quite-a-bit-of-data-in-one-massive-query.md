---
title: The faults when doing quite a bit of data in one massive query
origin: c6-Ways to Restructure Queries 
---

## Bad Example:

``` sql
DELETE FROM logs WHERE created < DATE_SUB(NOW(), INTERVAL 3 MONTH);
```

1. Could lock a lot of rows for a long time.

2. Fill up transaction logs

3. Hog resources

4. Block small queries that shouldn't be interrupted

## Good Example:

```
rows_affected = 0
do {
	rows_affected = do_query(
		"DELETE FROM messages WHERE created < DATE_SUB(NOW(),INTERVAL 3 MONTH) LIMIT 10000"
	)

	sleep(1)
} while rows_affected > 0
```

1. Chopping up the DELETE statement and using medium-size queries can improve performance considerably, and reduce replication lag when a query is replicated. 

2. A short enough task to minimize the impact on the server. (transactional storage engines might benefit from smaller transactions)

3. Add some sleep time to spread the load over time and reduce the amount of time locks are held.