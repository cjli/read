---
title: Access methods
origin: c6-Slow Query Basics_Optimize Data Access
---

MySQL can use several access methods to find and return a row, which appears in the `type` column in `EXPLAIN`'s output.

Read less data is better, from best to worst:

full table scan < index scans < range scans < unique index lookups < constants.

> Notes: `type` = 'ref', `ref` = 'const' stand for contansts.

If you aren’t getting a good access type, the best way to solve the problem is usually by adding an appropriate index. Indexes let MySQL find rows with a more efficient access type that examines less data.

MySQL does not tell you how many of the rows it accessed were used to build the result set; it tells you only the total number of rows it accessed. Many of these rows could be eliminated by a WHERE clause and end up not contributing to the result set.