---
title: The benefits from join decomposition
origin: c6-Ways to Restructure Queries 
---

Many high-performance applications use join decomposition. You can decompose a join by running multiple single-table queries instead of a multitable join, and then performing the join in the application. 

Bad example:

``` sql
SELECT * FROM tag
JOIN tag_post ON tag_post.tag_id=tag.id
JOIN post ON tag_post.post_id=post.id
WHERE tag.tag='mysql';
```

Good example:

``` sql
SELECT * FROM tag WHERE tag='mysql';
SELECT * FROM tag_post WHERE tag_id=1234;
SELECT * FROM post WHERE post.id in (123,456,567,9098,8904);
```

It looks wasteful at first glance, because you’ve increased the number of queries without getting anything in return. However, such restructuring can actually give significant performance advantages:

1. Caching can be more efficient.

- Many applications cache “objects” that map directly to tables.
- If only one of the tables changes frequently, decomposing a join can reduce the number of cache invalidations.

2. Executing the queries individually can sometimes reduce lock contention.

3. Doing joins in the application makes it easier to scale the database by placing tables on different servers.

- It's difficult to join tables across different databases in different servers.

4. The queries themselves can be more efficient.

- In this example, using an IN() list instead of a join lets MySQL sort row IDs and retrieve rows more optimally than might be possible with a join.

5. Reduce redundant row accesses and total network traffic and memory usage.

- Doing a join in the application means you retrieve each row only once, whereas a join in the query is essentially a denormalization that might repeatedly access the same data.
- For the same reason, such restructuring might also reduce the total network traffic and memory usage.

6. Algorithm efficiency.

- To some extent, you can view this technique as manually implementing a hash join instead of the nested loops algorithm MySQL uses to execute a join. A hash join might be more efficient.

As a result, doing joins in the application can be more efficient when you cache and reuse a lot of data from earlier queries, you distribute data across multiple servers, you replace joins with IN() lists on large tables, or a join refers to the same table multiple times.