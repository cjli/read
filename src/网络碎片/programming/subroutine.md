---
title: 子程序
origin: https://zh.wikipedia.org/wiki/%E5%AD%90%E7%A8%8B%E5%BA%8F
---


在计算机科学中，子程序（Subroutine, procedure, function, routine, method, subprogram, callable unit），是一个大型程序中的某部分代码，由一个或多个语句块组成。它负责完成某项特定任务，而且相较于其他代码，具备相对的独立性。

子程序一般会有输入参数并有返回值，提供对过程的封装和细节的隐藏。这些代码通常被集成为软件库。

子程序是一个概括性的术语，是所有高阶程序所称。它经常被使用在汇编语言层级上。子程序的主体（body）是一个代码区块，当它被调用时就会进入运行。

