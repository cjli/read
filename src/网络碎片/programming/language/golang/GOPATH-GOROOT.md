---
title: GOPATH vs GOROOT
---

GOPATH is discussed in the [cmd/go documentation](https://golang.org/cmd/go/#hdr-GOPATH_environment_variable):
The GOPATH environment variable lists places to look for Go code. On Unix, the value is a colon-separated string. On Windows, the value is a semicolon-separated string. On Plan 9, the value is a list.
GOPATH must be set to get, build and install packages outside the standard Go tree.

GOROOT is discussed in the [installation instructions](https://golang.org/doc/install#extra_versions):
The Go binary distributions assume they will be installed in /usr/local/go (or c:\Go under Windows), but it is possible to install the Go tools to a different location. In this case you must set the GOROOT environment variable to point to the directory in which it was installed.
For example, if you installed Go to your home directory you should add the following commands to $HOME/.profile:

export GOROOT=$HOME/go
export PATH=$PATH:$GOROOT/bin

Note: GOROOT must be set only when installing to a custom location.