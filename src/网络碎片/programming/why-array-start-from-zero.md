---
title: 为什么数组下标从 0 开始
origin: http://cenalulu.github.io/linux/why-array-start-from-zero/ 
---

1. 在计算资源缺乏的过去，0标号的写法可以节省编译时间。

2. 现代语言中0标号可以更优雅的表示数组字串。

3. 在支持指针的语言中，标号被视作是偏移量，因此从0开始更符合逻辑。
