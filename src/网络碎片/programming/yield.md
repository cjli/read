---
title: 什么是「协程」?
origin: V2EX 网友
---

> 线程包括开始、执行顺序和结束三部分。它有一个指令指针，用于记录当前运行的上下文。当其他线程运行时，它可以被抢占(中断)和临时挂起(也称为睡眠)——这种做法叫做让步(yielding)。（《Python核心编程》）

协程（ coroutine ）的协（ co ）是协作（ cooperative ）的意思，与之相对的是抢占式（ preemptive ）。

抢占式，就是轮到我执行了，现在执行的代码会被强制保存上下文然后暂停执行，换我执行。然而风水轮流转，我也会被这么抢占。

抢占的过程通常是 OS 管理的，所以有可能抢占的时机并不是很合适因为 OS 不知道你每个线程现在在做啥合不合适暂停，但好在可以保证所有代码都能得到执行。

协作式，就是我执行过程中，到了一个阶段发现没事可做了（通常是等待 IO ），这时候 yield，主动把 CPU 资源让出来让别的代码去执行。

同样的，别的代码也可能会在合适的时机主动暂停执行，让我再回来继续执行，如果我需要的其它资源已经到位（ IO 数据已经有了）。

协程的好处是这个让出的时机是开发时在代码里面明确写清楚了的（ yield 关键字），所以让出的时机可以更合理。

yield 带来两个主要结果：

1 ）主动暂停代码的执行，让渡自己的资源占用；

2 ）已经执行的代码可能产出一个中间成果。

而事实上 yield 这个词在英文的本意就正好带有这两个含义。

因为谈到协程通常是和抢占式的多线程相比，也就是更注重第一个含义 /结果，所以 yield 在协程这个概念里面的最好翻译是“让出”。当然最好是不翻译，直接用 yield。

说 yield 类似于 return 的，只看到了 yield 的第二个结果，忽略了第一个。
