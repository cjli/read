---
title: REST
origin: http://www.cnblogs.com/Leo_wl/p/4418663.html
---

一、定义

REST 是一种架构风格，而不是一种架构，一种架构风格可以用多种架构进行实现，一个架构中也可能包含多种架构风格，这两者的关系，你可以理解为抽象和实现的区别。

另外，REST 严格来说，应该属于 Web 架构的一种架构风格，因为它离不开 HTTP 协议。

二、核心概念

1. 资源

URI 仅仅是表述资源，URI 并不包含对这个资源的任何操作，所以，像 getUser、createUser 这类操作就不合适，资源的操作是通过 HTTP 提供的方法。

还有一点是，比如 POST 中的cnblogs.com/user 和 cnblogs.com/user/1 有什么不同？

第一种 POST，一般是创建一个新的 User 资源，创建完成后，一般会返回这样的一个 URI：cnblogs.com/user/1。
第二种 POST，不是说创建一个 Id 为 1 的 User 资源，而是在 Id 为 1 的 User 资源下创建某种资源，你会发现，好的 URI 设计应该不包含动词。

2. 状态

- 状态转化：如果客户端操作服务器，必须要状态转化，这个体现在表现层上，所以叫“表现层状态转化”。

- REST 是无状态的（Statelessness）：

* 状态分为：应用状态（Application State）和资源状态（Resource State）。
* 应用状态：与某一特定请求相关的状态信息。
* 资源状态：反映了某一存储在服务器端资源在某一时刻的特定状态。
* 客户端负责维护应用状态，而服务端维护资源状态。
* 服务器端不保有任何与特定 HTTP 请求相关的资源。

REST 中的无状态其实指的是应用状态，无状态的表现是服务端不保存应用状态，也就是客户端发起与请求相关的状态，应用状态是客户端在发起请求时提供的。

那状态转化是什么意思？其实指的是服务端资源状态的转化，表现在客户端中，也就是上面所说的“表现状态转化”。