---
title: 为什么 Ctrl+V 和 Ctrl+C 在 Linux 终端下不起作用
---

因为它们早于 Windows 被定义、而且不代表复制粘贴。