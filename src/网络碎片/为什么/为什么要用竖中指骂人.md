---
title: 为什么要用竖中指骂人
---

"竖中指"最早出现在英法百年战争末期。英国弓箭手让法军损失惨重，法军发誓在击败英军后，将英军弓箭手拉弓的中指斩断。但结果法军惨败。在法军撤退时，英军弓箭手纷纷伸出右手中指，炫耀他们依然存在的中指。这一侮辱性的手势迅速在西方国家"走红"。