---
title: Docker-中启用SSHD
---

Docker 原生不提供 /sbin/init 来启动 sshd 这类后台进程的，一个变通的办法是使用带 upstart 的根操作系统镜像，并将 /sbin/init 以 entrypoint 参数启动，作为 PID=1 的进程，并且严禁各种其他 CMD 参数。这样其他进程就可以成为 /sbin/init的子进程并作为后台服务跑起来。