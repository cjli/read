---
title: Zeros
origin: https://zhuanlan.zhihu.com/p/111643708
---

- MySQL Zero is 0000-00-00 00:00:00 UTC 
- Go Zero is    0001-01-01 00:00:00 UTC
- NTP Zero is   1900-01-01 00:00:00 UTC
- UNIX Zero is  1970-01-01 00:00:00 UTC