---
title: Don't waste time being frustrated with code 
origin: http://www.flyingmachinestudios.com/programming/learn-programming-languages-efficiently/
---

Frustration leads us into doing stupid things like re-compiling a program or refreshing the browser with the hope that this time it will be magically different.

If you've written some code that's not working, explicitly explain to yourself or someone else the result that you expected.

Use the scientific method and develop a hypothesis for what's causing the unexpected behavior.

Then test your hypothesis. Try again, and if a solution still eludes you, put the problem aside and come back to it later.
