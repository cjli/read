---
title: Identify the Purpose of Tool: External Model and Internal Model
origin: http://www.flyingmachinestudios.com/programming/learn-programming-languages-efficiently/
---

Whenever you’re learning to use a new tool, its useful to identify its purpose, external model and internal model.

When you understand a tool's purpose, your brain gets loaded with helpful contextual details that make it easier for you to assimilate new knowledge.

It's like working on a puzzle: when you're able to look at a picture of the completed puzzle, it's a lot easier to fit the pieces together.

This applies to languages themselves, and language libraries.

A tool's external model is the interface it presents and the way it wants you to think about problem solving.

> Clojure’s external model is a Lisp that wants you to think about programming as mostly data-centric, immutable transformations.

> Ansible wants you to think of server provisioning in terms of defining the end state, rather than defining the steps you should take to get to that state.

A tool's internal model is how it transforms the inputs to its interface into some lower-level abstraction.

> Clojure transforms Lisp into JVM bytecode.

> Ansible transforms task definitions into shell commands.
