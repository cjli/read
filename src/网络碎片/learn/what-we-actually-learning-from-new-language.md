---
title: Programming Language Aspect
origin: http://www.flyingmachinestudios.com/programming/learn-programming-languages-efficiently/
---

1. How to write code: syntax, semantics, and resource management.

2. The language's paradigm: object-oriented, functional, logic, etc.

3. The artifact ecosystem: how to build and run executables and how to use libraries.

4. Tooling: editors, compilers, debuggers, linters.
