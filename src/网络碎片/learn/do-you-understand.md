---
title: Do you understand
---

费曼: What I can not create, I do not understand.

爱因斯坦: 如果你不能给一个六岁小孩解释清楚，那你并不真的懂。