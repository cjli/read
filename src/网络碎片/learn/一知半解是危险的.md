---
title: 一知半解是危险的
origin: Alexander Pope（英国诗人、作家，1688-1744）
---

A little learning is a dangerous thing.