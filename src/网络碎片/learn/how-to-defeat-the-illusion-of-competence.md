---
title: How to defeat the illusion of competence?
origin: http://www.flyingmachinestudios.com/programming/learn-programming-languages-efficiently/
---

Testing effect constantly:

1. Before reading a chapter or watching a video, try guessing at what you're about to learn and write it down.

2. Try doing a chapter's exercises before reading the chapter.

3. Always do exercises, even the hard ones. It's OK to give up on an exercise and come back to it later (or never, even), but at least try it.

4. Read a short program and try to recreate it without looking at the original code. Or, go smaller and do this with a function.

5. Immediately after learning a new concept like objects, classes, methods, or higher-order functions, write code that demonstrates that concept.

6. Create diagrams that illustrate concepts, both in isolation and how they relate to each other.

7. Blog about a concept you just learned.

8. Try explaining the concept to a non-technical friend. Being able to explain an idea in layman's terms forces you to understand the idea deeply.
