---
title: What is business logic?
origin: https://www.reddit.com/r/rails/comments/77eesr/what_is_business_logic
---

The code that, when executed, does things that make people want to give you money.

