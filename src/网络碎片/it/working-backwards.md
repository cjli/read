---
title: Working Backwards
origin: https://www.allthingsdistributed.com/2006/11/working_backwards.html
---

The Working Backwards product definition process is all about is fleshing out the concept and achieving clarity of thought about what we will ultimately go off and build.

It typically has four steps:

1. Start by writing the Press Release.

> The press release describes in a simple way what the product does and why it exists - what are the features and benefits.

> It needs to be very clear and to the point.

> Writing a press release up front clarifies how the world will see the product - not just how we think about it internally.

2. Write a Frequently Asked Questions document.

> It includes questions that came up when we wrote the press release.

> You would include questions that other folks asked when you shared the press release and you include questions that define what the product is good for.

3. Define the customer experience.

> For products with a user interface, we would build mock ups of each screen that the customer uses.

> For web services, we write use cases, including code snippets, which describe ways you can imagine people using the product.

> The goal here is to tell stories of how a customer is solving their problems using the product.

4. Write the User Manual.

> The user manual is what a customer will use to really find out about what the product is and how they will use it.

> The user manual typically has three sections, concepts, how-to, and reference, which between them tell the customer everything they need to know to use the product.

> For products with more than one kind of user, we write more than one user manual.

5. Build.

> Once we have gone through the process of creating the press release, faq, mockups, and user manuals, it is amazing how much clearer it is what you are planning to build.

> We'll have a suite of documents that we can use to explain the new product to other teams within Amazon.
