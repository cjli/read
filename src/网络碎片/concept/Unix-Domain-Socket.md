---
title: 什么是UNIX Domain Socket？
origin: 架构师之路--58沈剑 
---

什么是UNIX Domain Socket？ UNIX Domain Socket是一种IPC机制，它不需要经过网络协议栈，不需要打包拆包、计算校验和、维护序号和应答等，只是将应用层数据从一个进程拷贝到另一个进程。
画外音：IPC, Inter-Process Communication, 进程间通信。

它可以用于同一台主机上两个没有亲缘关系的进程，并且是全双工的，提供可靠消息传递（消息不丢失、不重复、不错乱）的IPC机制。
画外音：亲缘关系是指，父子进程或者兄弟进程这种“特殊的”进程关系。

可以看到，UNIX Domain Socket的效率会远高于tcp短连接，但它只能用于同一台主机间的进程通信。