---
title: BrainFuck
origin: https://zh.wikipedia.org/wiki/Brainfuck
---

Brainfuck，是一种极小化的计算机语言，它是由Urban Müller在1993年创建的。由于fuck在英语中是脏话，这种语言有时被称为 `Brainf*ck` 或 `Brainf***`，或被简称为BF。

Müller的目标是创建一种简单的、可以用最小的编译器来实现的、匹配图灵完全思想的编程语言。这种语言由八种运算符构成，为Amiga机器编写的编译器（第二版）只有240个字节大小。

就象它的名字所暗示的，Brainfuck程序很难读懂。尽管如此，Brainfuck图灵机一样可以完成任何计算任务。虽然Brainfuck的计算方式如此与众不同，但它确实能够正确运行。
