---
title: HSTS
origin: https://zh.wikipedia.org/wiki/HTTP%E4%B8%A5%E6%A0%BC%E4%BC%A0%E8%BE%93%E5%AE%89%E5%85%A8
---

HSTS 的作用是强制客户端（如浏览器）使用 HTTPS 与服务器创建连接。

服务器开启 HSTS 的方法是，当客户端通过 HTTPS 发出请求时，在服务器返回的超文本传输协议响应头中包含 `Strict-Transport-Security` 字段。非加密传输时设置的 HSTS 字段无效。

比如，https://example.com/ 的响应头含有 `Strict-Transport-Security: max-age=31536000; includeSubDomains`。这意味着两点：

- 在接下来的一年（即31536000秒）中，浏览器只要向example.com或其子域名发送HTTP请求时，必须采用HTTPS来发起连接。比如，用户点击超链接或在地址栏输入 http://www.example.com/ ，浏览器应当自动将 http 转写成 https，然后直接向 https://www.example.com/ 发送请求。

- 在接下来的一年中，如果 example.com 服务器发送的 TLS 证书无效，用户不能忽略浏览器警告继续访问网站。