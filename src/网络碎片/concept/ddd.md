---
title: 领域驱动设计/开发
origin: 《Domain-Driven Design:Tackling Complexity in the Heart of Software》, Eric Evans
---

Domain-driven design(DDD)，是一种通过将实现连接到持续进化的模型来满足复杂需求的软件开发方法。

领域驱动设计的前提是：

- 把项目的主要重点放在核心领域（core domain）和域逻辑

- 把复杂的设计放在有界域（bounded context）的模型上

- 发起一个创造性的合作之间的技术和域界专家以迭代地完善的概念模式，解决特定领域的问题

> 关注点分离。

