---
title: Ad hoc
origin: https://zh.wikipedia.org/wiki/Ad_hoc
---

Ad hoc 是拉丁文常用短语中的一个短语。这个短语的意思是“特设的、特定目的的（地）、即席的、临时的、将就的、专案的”。这个短语通常用来形容一些特殊的、不能用于其它方面的，为一个特定的问题、任务而专门设定的解决方案。这个词汇须与 a priori 区分。

这个短语通常用来形容的东西包括一些为一个特别问题而设立的国家级或者国际性的组织、组委会以及合约等。在其他方面，譬如特殊环境下所组建的军队单位、手工制作的服装、临时搭建的网络协议或者是满足特殊条件的方程式等也可使用这个短语来进行形容。

Ad hoc 同时也包含有负面的评价，表明所形容的事物是一个权宜之计、不周密的计划或者是一个即兴举办的活动。

举例说明：

- Ad hoc （非常设）委员会、合约或组织
- Ad hoc 假设（又作“特例假设”或者“特设性假设”）
- Ad hoc 查询（又作“即席查询”）
- Ad hoc 军事（又作“特设军事”）
- Ad hoc 网络（又作“临时网络”、“自组网”或“随建即连网络”）
