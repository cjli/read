---
title: Richardson Maturity Model
origin: https://martinfowler.com/articles/richardsonMaturityModel.html
---

RMM: To help explain the specific properties of a web-style system, the authors use a model of restful maturity that was developed by Leonard Richardson and explained at a QCon talk.

Roy Fielding has made it clear that level 3 RMM is a pre-condition of REST. Like many terms in software, REST gets lots of definitions, but since Roy Fielding coined the term, his definition should carry more weight than most.

- level 0: Using HTTP as a transport system for remote interactions.

- level 1: Resources.

Tackles the question of handling complexity by using divide and conquer, breaking a large service endpoint down into multiple resources.

- level 2: HTTP Verbs + Response Status Code.

REST advocates talk about using all the HTTP verbs, together with using status codes to help communicate the kinds of errors you run into.

> At Level 2, the use of GET for a request like this is crucial. HTTP defines GET as a safe operation, that is it doesn't make any significant changes to the state of anything. This allows us to invoke GETs safely any number of times in any order and get the same results each time. 

Level 2 introduces a standard set of verbs so that we handle similar situations in the same way, removing unnecessary variation.

level 3: Hypermedia Controls

Introduces discoverability, providing a way of making a protocol more self-documenting.

> HATEOAS: Hypertext As The Engine Of Application State.

The point of hypermedia controls is that they tell us what we can do next, and the URI of the resource we need to manipulate to do it. Rather than us having to know where to post our appointment request, the hypermedia controls in the response tell us how to do it.

One obvious benefit of hypermedia controls is that it allows the server to change its URI scheme without breaking clients. 

A further benefit is that it helps client developers explore the protocol. The links give client developers a hint as to what may be possible next, it gives them a starting point as to what to think about for more information and to look for a similar URI in the protocol documentation.