---
title: What is XY Problem ?
origin: https://perl.plover.com/Questions3.html
tag: XY
---

Someone wants to accomplish task Y.

They determine that a way to accomplish Y is to use facility X.

There is a sub-task of Y, say Z, for which X is ill-suited.

They come into the group and ask: how can I use X to accomplish Z?

