---
title: 英文全字母句
origin: https://zh.wikipedia.org/wiki/%E5%85%A8%E5%AD%97%E6%AF%8D%E5%8F%A5
---

The quick brown fox jumps over the lazy dog（敏捷的棕毛狐狸从懒狗身上跃过）。

该句是一个著名的英语全字母句，常被用于测试字体的显示效果和键盘有没有故障。

此句也常以“quick brown fox”做为指代简称。

