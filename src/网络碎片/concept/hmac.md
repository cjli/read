---
title: hmac
---

Hash-based Message Authentication Code，基于hash 运算的消息认证码。

消息认证的三种方式：

- 消息认证码：它是一个需要密钥的算法，可以对可变长度的消息进行认证，把输出的结果作为认证符。

- 散列函数：它是将任意长度的消息映射成为定长的散列值的函数，以该散列值消息摘要）作为认证符。

- 消息加密：它将整个消息的密文作为认证符

人们越来越感兴趣于利用散列函数来设计MAC，原因有二：

- 一般的散列函数的软件执行速度比分组密码的要快。

- 密码散列函数的库代码来源广泛。

因此 HMAC 应运而生，HMAC是一种利用密码学中的散列函数来进行消息认证的一种机制，所能提供的消息认证包括两方面内容：

- 消息完整性认证：能够证明消息内容在传送过程没有被修改。

- 信源身份认证：因为通信双方共享了认证的密钥，接收方能够认证发送该数据的信源与所宣称的一致，即能够可靠地确认接收的消息与发送的一致。

在 HMAC 规划之初，就有以下设计目标：

- 不必修改而直接套用已知的散列函数，并且很容易得到软件上执行速度较快的散列函数及其代码。
- 若找到或需要更快或更安全的散列函数，能够容易地代替原来嵌入的散列函数。
- 应保持散列函数的原来性能，不能因为嵌入在HMAC中而过分降低其性能。
- 对密钥的使用和处理比较简单。
- 如果已知嵌入的散列函数强度，则完全可以推断出认证机制抵抗密码分析的强度。