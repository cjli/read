---
title: MVPs
origin: https://zh.wikipedia.org/wiki/MVP
---

Most Valuable Player: 最有价值球员。

Model-View-Presenter: 软件工程设计与架构样式。

Most Valuable Professor: 最有价值专家，微软公司评选的对微软产品专业知识了解并积极参与社区讨论的用户。

Minimum Viable Product: 最简可行产品。
