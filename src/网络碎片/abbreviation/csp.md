---
title: Communicating sequential processes
origin: https://en.wikipedia.org/wiki/Communicating_sequential_processes
---

交谈循序程序，又称为通信顺序进程、交换消息的循序程序，一种形式语言，用来描述并发性系统间进行交互的模式。

最早起源于东尼·霍尔在1978年发表的论文，之后又经过一系列的改善。但在出现之后，就成为描叙并发程序设计中常用的形式语言。

通信顺序进程高度影响了Occam的设计，也影响了如Limbo与Go等编程语言。

