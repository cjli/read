---
title: Site Reliability Engineering
origin: https://landing.google.com/sre/#sre
---

SRE is what you get when you treat operations as if it’s a software problem.

Our mission is to protect, provide for, and progress the software and systems behind all of Google’s public services — Google Search, Ads, Gmail, Android, YouTube, and App Engine, to name just a few — with an ever-watchful eye on their availability, latency, performance, and capacity.

