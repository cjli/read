---
title: Computer Systems - A Programmer's Perspective
origin: https://book.douban.com/subject/5333562/
---

《深入理解计算机系统》。

一本想要深刻理解计算机科学的人必读、练习的教材之一，主要从程序员视角详细阐述了计算机系统的本质，以及如何实实在在地影响者程序。
