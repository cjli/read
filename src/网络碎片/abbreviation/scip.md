---
title: Structure and Interpretation of Computer Programs
origin: <https://book.douban.com/subject/1148282/>
---

《计算机程序的构造和解释》。

一本想要深刻理解计算机科学的人必读、练习的教材之一，主要讨论了计算机程序设计的思想。

