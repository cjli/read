---
title: 如果我想要，我必须得到它 
origin: Mlle. Modiste, Act2, Henri de Bouvray
---

I want what I want when I want it.

起源是是歌剧秀“摩蒂斯小姐” 中第二场表演中的一段剧本主题。
