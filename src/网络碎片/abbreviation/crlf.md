---
title: CRLF
origin: https://zh.wikipedia.org/wiki/%E6%8F%9B%E8%A1%8C
---

换行的表示方式有：newline, Line Break, end-of-line(EOF), Line Feed(LF)。

CRLF: Carriage Return & Line Feed，回车换行，DOS/Windows 采用此换行方式。

LF: 在Unix或Unix兼容系统（GNU/Linux，AIX，Xenix，Mac OS X，...）、BeOS、Amiga、RISC OS。

CR：Apple II家族，Mac OS至版本9。

