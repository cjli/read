---
title: 超链接中的 utm_* 代表什么意思？
origin: https://www.zhihu.com/question/48724061
---

UTM：urchin tracking module。

作用：UTM 除了最基础的追踪流量来源外，还可以根据不同渠道、不同内容做精细化运营分析，帮你对比区分优质和劣质渠道，提高流量在产品内的转化。

历史：最早是Google analytics用来定义链接来源 媒介等使用的参数以方便在分析用户站内行为时分析用户来源的效果。至于为什么要叫 UTM 故事要从十几年前讲起，当时还没有 Google analytics，但是有一个叫 urchin 的网站分析工具，后来google 收购了 urchin 的这个产品并改名叫 Google analytics。但是 UTM 这个名字因为大家已经习惯，就一直保留下来并被大量其它后起的网站监测工具使用（国内的如Growing IO，百度统计等）

常见例子：

- utm_source 用来定义来源，如baidu，weibo，wechat （obligation）
- utm_medium 用来定义媒介方式 如 email，cpc，cpmutm_campaign 用来定义campaign名称
- utm_content 用来定义内容
- utm_term 用来定义campaign的关键词
