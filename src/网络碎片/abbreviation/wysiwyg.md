---
title: 所见即所得
origin: https://en.wikipedia.org/wiki/WYSIWYG
---

WYSIWYG (/ˈwɪziwɪɡ/ WIZ-ee-wig) is an acronym for "what you see is what you get".

In computing, a WYSIWYG editor is a system in which content (text and graphics) can be edited in a form closely resembling its appearance when printed or displayed as a finished product, such as a printed document, web page, or slide presentation.

In general, WYSIWYG implies the ability to directly manipulate the layout of a document without having to type or remember names of layout commands.

The actual meaning depends on the user's perspective, e.g.
