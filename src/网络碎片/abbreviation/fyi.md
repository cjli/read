---
title: FYI
origin: https://en.wikipedia.org/wiki/FYI
---

FYI is a common abbreviation of "For Your Information".（提供给您的信息，供您参考）

"FYI" is commonly used in e-mail, instant messaging or memo and messages, typically in the message subject, to flag the message as an informational message, with the intent to communicate to the receiver that he/she may be interested in the topic, but is not required to perform any action.

It is also commonly used in informal and business spoken conversations.

