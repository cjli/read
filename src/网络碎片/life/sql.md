---
title: SQL of life
origin: 脉脉网友
---

SELECT
    CASE
        WHEN your power cannot support ambition THEN improve yourself and make you better_and_better
        WHEN your power stronger enough THEN you can do whatever_you_want
        ELSE go to glory
    END life_result
FROM the_rest_of_your_life_always;

