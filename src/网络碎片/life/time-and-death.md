---
title: 死亡和时间
origin: @豆瓣文章
---

生命是一个消耗的过程，大多数人都不能活到一百岁，我们每过去一天，就意味着临死亡又近了一条。

很多人敬畏生命，恐惧死亡，但很少有人会敬畏时间，反而不把时间当回事，任其流失。

余生看着很长，也有很多人大器晚成，但真正能够让你放开手脚折腾的就那么几年时间，就是年轻时的那段日子。