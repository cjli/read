---
title: 穷人与富人
origin: 简书文章
---

我努力让自己从一个打工族变成了一个自由职业者，努力让自己的被迫收入变为主动收入，我开始见识更多，接触更多，我终于明白了有钱人为什么越来越有钱，有钱了为什么还要努力赚钱。

因为，那种态度，那种思维方式，穷人跟富人最本质的区别是就在于他们的思维方式。这个世界没有什么存在是最好的，因为还有更好的。

全球最富有的人巴菲特曾说过：真正富有的人生，是做着一份自己喜欢的、并愿意为之奋斗到生命最后一刻的工作。
