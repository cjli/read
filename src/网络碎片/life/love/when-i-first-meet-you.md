---
title: When I first meet you 
---

When I first meet you.

You were nothing more than another face.

But 6 months after and you're the only face I could ever find in a crowded place.