---
title: Beans and Rice - A vegetarian love song  
origin: https://hullabaloo.bandcamp.com/track/beans-and-rice
---

I love you once, I love you twice.

> 一见倾情，再见倾心。

I love you more than beans and rice.

> 我爱你，此生不渝。

I love you blue, I love you green.

> 红尘作伴，甘难与共。

I love you more than peach ice cream.

> 我爱你，情比金坚。

I love you north, south, east and west.

> 天荒地老，海枯石烂。

You’re the one I love the best.

> 你永远是我的唯一。

