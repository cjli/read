---
title: 君生我未生
origin: 《铜官窑瓷器题诗二十一首》，唐代
---

君生我未生，我生君已老。恨不生同时，日日与君好。

I was not when you were born.

You were old when I was born.

You regret that I was late born.

I regret that you were early born.

I wished to have been born together.

We could enjoy our time together.

