---
title: 面具朋友
origin: 豆瓣文章
---

一个人有许多面，在不同的人面前展示的自我都不相同。有时候我们迫于生存压力，不得不伪装自己，戴上符合别人期待的面具，以便被人接纳。

我们不得不“装”的同时，也有人乐于看见我们的“装”，这类人就是“面具朋友”。他只愿意看到自己喜欢看到的、符合自己期待的你，却认为接纳真实的你是一种麻烦。

面具朋友往往是那些一开始让我们觉得：哇，这个人认为似乎被我吸引了。实际上，只是当我们施展魅力的时候，他们才会被我们吸引，一旦我们不小心暴露了真实的自己，他们便会用实际行动告诉我们：其实我们的关系也没有那么深，你在我面前不应该如此坦诚。
