---
title: 陪伴与失去
origin: 豆瓣文章
---

离家六七年了，虽然每年回家一两次，但你确实能感觉到某种缓慢地失去。毕竟有些东西是要靠陪伴才能获得的。

比如孩子的成长，比如对父母的孝心。

在某个不经意的时刻，你想起这些，会有种撕心裂肺的酸楚。

