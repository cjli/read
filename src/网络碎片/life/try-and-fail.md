---
title: 屡战屡败，屡败屡战
origin: 彼特·丁拉基（Peter Dinklage）
---

Ever tried, Ever failed.

No matter. Try again.

Fail again. Fail better.

The world is yours.

> 屡战屡败 

> 屡败屡战

> 再失败，失败得更加精彩

> 全世界都是你的

