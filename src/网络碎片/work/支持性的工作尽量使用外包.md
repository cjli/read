---
title: 支持性的工作尽量使用外包
origin: https://www.infoq.cn/article/fVAkBmhfwVaQ7868BT3g
---

一些支持性的工作尽可能地使用外包，比如：HR、行政、发工资、财务、员工持股、测试人员、定制化开发……

这样可以让你的团队更小，更高内聚，更利于远程。