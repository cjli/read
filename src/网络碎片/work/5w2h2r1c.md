---
title: 开会时如何记录工作任务安排？
---

工作任务的安排，可参考 5W2H2R1C。

- Why、Who、When、Where、What？

为什么要做、希望谁做、在什么时候做、在什么地方做、做什么？

- How、How Much?

希望这么做、做到何种程度？

- Resource、Result?

有什么资源支持、希望获得什么结果？

- Confirm

复述你的记录，确认理解无误。
