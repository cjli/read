---
title: CV-vs-Resume
origin: https://zhuanlan.zhihu.com/p/24832194
---

CV 和Resume有很多相似之处，但是CV更注重学术成就。 CV扼要地总结作者的教育及学术史，同时包括教学经验，论文，学术奖项。

从这个特点来看我们在申请西方大学时需要提供的是CV而不是Resume (除了那些将CV和Resume混为一谈的学校)。

CV长于Resume. 因为前者强调申请人的全面性(completeness), 而后者强调申请人特点的简洁性(brevity)。