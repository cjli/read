---
title: 优秀员工应该是怎样的
origin: 脉脉网友
---

一个优秀的员工应该是怎么样的？

1.有很强的主动性，拥有主动做事情的动机和行为，不是在公司加班耗时间，而是想办法提前工作效率，思考如何将工作完成的更好。
2.有主人翁精神，热爱自己的工作，认可和珍惜自己的产品，愿意和团队成员一起对产品持续优化改进，并以此为荣。
3.有领导力，愿意钻研技术，有攻克疑难问题的精神和决心，愿意不断寻找针对产品和技术的更好的方案。