---
title: 人脉只会给你机会
---

不要去听别人的忽悠，你人生的每一步都必须靠自己的能力完成。

自己肚子里没有料，手上没本事，认识再多人也没用。

人脉只会给你机会，但抓住机会还是要靠真本事。

修炼自己，比到处逢迎别人重要的多。

人一复杂就疲惫。人一简单，就会很快乐。复杂永远拼不过简单。