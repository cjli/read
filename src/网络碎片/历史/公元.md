---
title: 公元
---

## 中国传统历法「元年」

按照华夏始祖“轩辕黄帝”登基为元年。

## 西方纪年「公元」

公元，即公历纪元，原称基督纪年，又称西历或西元、格里高利历。一种源自于西方社会的纪年方法，是由意大利医生兼哲学家 Aloysius 
Lilius 对儒略历加以改革而制成的一种历法。1582 年，时任罗马教皇的格列高利十三世予以批准颁行。

它以耶稣诞生之年作为纪年的开始，纪年的起点是公元 1 年。

## 公元前公元后

在儒略历与格里高利历中，在耶稣诞生之后的日期，称为主的年份（A.D.），而在耶稣诞生之前，称为主前（B.C.）。

但是现代学者为了淡化其宗教色彩以及避免非基督徒的反感，而多半改称用公元（C.E.）与公元前（ 
B.C.E.）。

我国传统历法比西方历法早 2697 年，用「西元年份+2697」即是我国传统历法的年份。

举例说明：公元 2018.02.16~2019.02.04 这一年是农历开元 4715 戊戌狗年。

## 中华民国纪年法

公元 1911 年，辛亥革命爆发。次年中国开始使用公历。

中华民国政府采用公历作为国历，纪年方面，公元纪年法与民国纪年法并行。公元 1992 年即民国 1 年。

## 干支纪年法

从黄帝纪年（元年）开始，在相当长的历史时期内，中国使用的是“干支纪年法”，即把十天干和十二地支分别组合起来，每 60 年为一个周期。