---
title: 两相思
origin: 李禺（宋）
---

正读：思妻诗

枯眼望遥山隔水，往来曾见几心知?
壶空怕酌一杯酒，笔下难成和韵诗。
途路阳人离别久，讯音无雁寄回迟。
孤灯夜守长寥寂，夫忆妻兮父忆儿。

反读：思夫诗

儿忆父兮妻忆夫，寂寥长守夜灯孤。
迟回寄雁无音讯，久别离人阳路途。
诗韵和成难下笔，酒杯一酌怕空壶。
知心几见曾往来，水隔山遥望眼枯。

> 这种诗体叫做回文诗，它是一种可以倒读或反复回旋阅读的诗体。
