---
title: Common problems with CGI scripts
origin: https://docs.python.org/2/howto/webservers.html 
---

1. The Python/CGI script is not marked as executable. 

2. Line Break style. On a Unix-like system, The line endings in the program file must be Unix style line endings.

3. Your web server must be able to read the file, and you need to make sure the permissions are correct.

4. The web server must know that the file you’re trying to access is a CGI script. Check the configuration of your web server, as it may be configured to expect a specific file extension for CGI scripts.

5. On Unix-like systems, the path to the interpreter in the shebang (like `#!/usr/bin/env python`) must be correct.

6. The file must not contain a BOM (Byte Order Mark).

> The BOM is meant for determining the byte order of UTF-16 and UTF-32 encodings, but some editors write this also into UTF-8 files.
> The BOM interferes with the shebang line, so be sure to tell your editor not to write the BOM. 

7. If the web server is using mod_python, mod_python may be having problems. mod_python is able to handle CGI scripts by itself, but it can also be a source of issues.
