---
title: 网站分析指标
---

- QPS/每秒查询率

QPS = req/sec = 请求数/秒。每秒的响应请求数，也即是最大吞吐能力。

QPS = 总请求数 / ( 进程总数 * 请求时间 )。

QPS = 单个进程每秒请求服务器的成功次数。

- PV/页面浏览量

Page View，又称点击率，是衡量一个网站的重要指标。用户一次刷新或一次地址访问立即被计算一次加 1。

计算公式：每天总PV = QPS * 3600 * 每日营业时间长度。（每天6～8小时左右） 

- UV/独立访客量

Unique Visitor，访问您网站的一台电脑客户端为一个访客(不同IP地址的人数)。同一天 00:00-24:00 内相同的客户端只被计算一次，独立 IP 访问者提供。

- Throughput/吞吐量

吞吐量是指系统在单位时间内处理请求的数量。

- RT/响应时间

响应时间是指系统对请求作出响应的时间。

- 并发数用户数

并发用户数是指系统可以同时承载的正常使用系统功能的用户的数量。

- PR/网页排名

PageRank，即网页的级别技术，或网站权重或受欢迎度。表示一个网页的重要程度。级别从1到10级，10级为满分。PR值越高说明该网页越受欢迎。
