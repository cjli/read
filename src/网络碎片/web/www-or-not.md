---
title: WWW or not? 
origin: https://bjornjohansen.no/www-or-not
---

1. www: Humans’ understanding of an URL.

2. Non-www is prettier and easier.

3. Cookies are passed down to subdomains.

- Unnecessary cookies hurts performance.
- The cookies can be read by third parties.
- Cookies from subdomains, can be shared – if you want to.

4. DNS origin can’t be a CNAME.

- DNS origin, must be an A type record.
- There is issue with Non-www hostname(top domain) being a CNAME.

5. With www: Perfomance-wise, security-wise and in terms of flexibility.