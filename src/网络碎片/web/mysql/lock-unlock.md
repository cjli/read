---
title: LOCK & UNLOCK
---

LOCK TABLES 为当前会话锁定表，UNLOCK TABLES 释放被当前会话持有的任何锁，但是当会话发出另外一个 LOCK TABLES 时，或当服务器的连接被关闭时，当前会话锁定的所有表会隐式被解锁。

LOCK TABLES 与 UNLOCK TABLES 只能为自己获取锁和释放锁，不能为其他会话获取锁，也不能释放由其他会话保持的锁。