---
title: MySQL 优化顺序
origin: https://www.zhihu.com/question/19719997
---

1. 优化你的 SQL 和索引。

2. 加缓存：memcached，redis。

3. 主从复制或主主复制，读写分离。

4. 尝试 MySQL 自带分区表。

> 对应用透明，无需改代码，但 SQL 需要针对分区表优化。

5. 垂直拆分、分布式、微服务。

6. 水平切分。

> 选择合适的 sharding key；表结构改动；一定冗余。

说明：排序分先后，不要为了技术而技术。