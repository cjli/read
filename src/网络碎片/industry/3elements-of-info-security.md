---
title: CIA - 信息安全三要素
origin: https://site.douban.com/252830/widget/notes/18662977/note/498741695/
---

Confidentiality – the property that information is made available or disclosed to unauthorized individuals, entities or processes.

> 保密性 – 信息被获取或泄漏给未经授权的个人、实体或流程

Integrity – the property of safeguarding the accuracy and completeness.

> 完整性 – 保护资产准确和完整

Availability – the property of being accessible and useable upon demand by an authorized entity.

> 可用性 – 资产仅对授权人员在需要的时候是可访问的或可用的
