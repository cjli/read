---
title: 删除 GitHub 仓库的 Wiki
---

```
git clone git@github.com:ACCOUNT/REPO.wiki.git
cd REPO.wiki
git checkout --orphan empty
git rm --cached -r .
git commit --allow-empty -m 'wiki deleted'
git push origin empty:master --force
```

然后到仓库的设置地方取消勾选 Wiki 功能。