---
title: 活着是为了成就自己
origin: Amazon Kindle 买家
---

我们或者不是为了工作，不是为了做设计、写程序，这些不是我们生活的目的。

我们活着是为了成就我们自己，而要想成就自己，就必须首先成就他人。

每个人都有自己成就的目标，而工作是达成自我成就的一种手段。通过工作的挑战，发掘自我的潜能，重新认知自我和世界。

软件开发不仅是制造软件的过程，也是开发人员完善自我、超越自我的过程。

所以我们工作不只是生产产品，还有成就别人，最终成就我们自己。

