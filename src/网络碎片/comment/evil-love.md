---
title: Evil Love
origin: 网易云音乐网友
---

记得有一句话是这么说的：

> 想和你谈的恋爱是那种坏人和坏人之间的。

> 是两个薄情寡义心怀鬼胎悲观主义者之间的，是两个千帆过尽金戈铁马浪子之间的。

> 我想和你互相看尽对方的底牌，了解彼此的阴暗，然后我们依然相爱。

> 有时候爱不是什么真善美，只是你随手拧开煤气灶，我笑着划了一根火柴。

