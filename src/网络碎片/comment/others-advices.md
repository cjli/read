---
title: Advices from others 
origin: Reddit 网友
---

Tip #0: Don't take anyone else's advice too seriously. Most people give advice to validate their own choices and do not really take into account others' experiences, values, and struggles.

