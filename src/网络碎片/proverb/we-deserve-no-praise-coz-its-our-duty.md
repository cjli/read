---
title: We deserve no praize when it's our duty
origin: Saint Augustine
---

In doing what we ought, we deserve no praise, because it is our duty.
