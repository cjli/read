---
title: fool make thing complicated
origin: Woody Guthrie 
---

Any fool can make something complicated. It takes a genius to make it simple.