---
title: 一朝被蛇咬，十年怕井绳
origin: 续传灯录
---

Once burned, twice shy and all.

一度著蛇咬，怕见断井索。

一年被蛇咬，三年怕草索。

> 比喻：遭过一次伤害以后就害怕遇到同样或类似的事物、事件。
