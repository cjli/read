---
title: break
origin: 百词斩爱阅读
---

1. break up：结束、分手、破碎等和分开相关的意思。

- Let's make a pact. We don't break up.
- Clearly a hug can solve the problem. It is said to break up.（明明一个拥抱就能解决的问题，偏偏要说分手）

2. break down：表损坏、身体垮掉等看起来向下、倒掉的意思。

- Their car broke down.
- Will you break down these walls and pull me through?

3. break in：打断（对话）、闯入等意思。

- Robbers broke in.
- If you break in on someone's conversation, you interrupt them.

4. break out：爆发、逃出。

- He was young when war broke out.
- The two men broke out of their cells and cut through a perimeter fence.
