---
title: since 连词用法
origin: 百词斩爱阅读
---

since 作连词时，有两个意思：

1. 表时间：自从（某时起，就一直怎么样）。注意，从句一般用过去时态，主句用完成时态（have done/had been doing）。比如：

She had been talking to her mother about the lives of homeless people since they first saw the homeless man.

2. 表原因：由于、既然（同 as）。比如：

- Since we were 5, we have been friends.
- Since they're quite hard to find, these stones are rather expensive.

since 子句可以也放句末。即：

- We have been friends since we were 5.
- These stones are rather expensive since they're quite hard to find.
