---
title: “笑”
origin: 百词斩爱阅读
---

1. smile：最常用的“笑”，表示面露微笑，侧重于无声。

2. laugh：表示大笑，嘲笑等。

- He laughed with pleasure when people said he looked like his dad.（人们说他长得像父亲时，他乐得大笑起来）
- You can stand there and feel superior as you point and laugh at them.（你站在那儿指指点点、嘲笑他们时，会觉得自己高人一等）

3. giggle：表示傻笑、咯咯笑，通常用于女人和孩子。比如：

- Both girls began to giggle.（两个女孩都咯咯地笑了起来）
- She gave a little girlish giggle.（她像个小女孩一样咯咯地笑了起来）

4. grin：表示咧嘴笑、露齿而笑。

- He grins, delighted at the memory.（想到往事，他开心地咧嘴笑了起来）
- Bobby looked at her with a sheepish grin.（博比望着她腼腆地咧嘴一笑）

5. beam：常用于书面用语，指人因心情舒畅而发出的笑，即喜形于色地笑。比如：

- Frances beamed at her friends with undisguised admiration.（费朗西开心地对自己的朋友笑着，毫不掩饰她的羡慕之情）
- "Welcome back", she beamed.（“欢迎回来！”，她满面笑容地说）

6. sneer：指冷笑、嘲笑。比如：

- "Hypocrite", he sneered.（“伪君子！”，他不屑地说）
- A faint sneer hangs on the lips.（嘴角上挂着一丝冷笑）
