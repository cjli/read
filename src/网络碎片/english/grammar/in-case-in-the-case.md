---
title: in case != in the case
origin: 百词斩爱阅读
---

1. in the case of：在...情形下，在...例子中。比如：

- in the case of sick leave（在病假的情况下）
- in the case of his health（就他的健康看来）

2. in that/this case：介词短语，表“在那样/这样的情形下”。比如：

- In that case, I prefer to stay on the bus.

3. in case：连词，连接主句和从句，表“以防、万一、如果”。比如：

- I have also sent you the book in case you need it.
- Bring a map in case you get lost.

4. in case of：意思和 in case 一样，只是 in case of 后接名词。比如：

Break the glass in case of fire.（万一着火，就敲碎玻璃）
