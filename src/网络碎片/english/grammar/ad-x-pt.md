---
title: 被 ad?pt 裹住的单词
origin: 百词斩爱阅读
---

1. adapt：适应、改变、改写。

2. adopt：收养、采用、批准。

3. adept：精于、擅长、专家。
