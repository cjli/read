---
title: all 有关的短语
origin: 百词斩爱阅读
---

1. above all：顾名思义，在所有之上，即首先、最重要的是。比如：

- Above all, chairs should be comfortable.
- Above all, he loves his work.

2. after all：归根结底、毕竟、终究等。比如：

- He failed after all.
- After all, I am defeated by him.

3. at all：根本，简直。一般和否定词一起连用。比如：

- It is no benefit to me at all.
- I feel no sorry for it at all.

4. in all：总共、共计。比如：

- Six hours were lost in all.
- In all, she has eight costume changes.
