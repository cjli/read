---
title: give
origin: 百词斩爱阅读
---

1. give up：放弃，衍生出投降、把...让给等意思。比如：

If you give up something, you stop doing it or having it.（如果你放弃某件事，你就停止做或者拥有它）

2. give in：屈服，衍生出让步、投降等意思。比如：

I refuse to give up, and I refuse to give in.

3. give off：放出，衍生出发出、散放出、发射出等意思。比如：

The apples give off a very sweet smell.

4. give out：发出，衍生出公布、付出等意思。比如：

The most important thing in life is to learn how to give out love, and to let it come in.

give out 和 give off 意思相近，但是侧重点不同。

- give off 通常用来表示“散发、发出”某种气体或者气味。
- give out 通常用来表述“散发”某种光、声、热、信号等物理现象。此外，还可以表示“分发、宣布、耗尽、付出”等意思。

5. give back：归还。

All energy is only borrowed, and one day you have to give it back.

6. give away：赠送、泄露、失去等。

Who will be willing to give away any loved ones?
