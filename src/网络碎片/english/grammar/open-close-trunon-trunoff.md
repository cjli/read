---
title: 打开与关闭
origin: 百词斩爱阅读
---

在英语里，至少有 2 组开关。

1. open/close：将原来合上的东西打开，或者将原来打开的东西合上。open 只能和 close 对应。

- open your eyes/heart/arms
- open the window/door/book

2. turn on /trun off：指转动或推上开关灯打开、关闭。比如：

- turn on the tv/light/switch
- turn on the water/shower/tap
