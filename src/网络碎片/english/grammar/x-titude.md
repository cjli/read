---
title: 以 titude 结尾的单词
origin: 百词斩爱阅读
---

1. attitude：看法、态度。

2. altitude：高度、海拔。

3. latitude：纬度、范围。

4. aptitude：才能、资质。

5. gratitude：感激、感谢。
