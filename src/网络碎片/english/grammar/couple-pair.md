---
title: 很“2”的一对儿：couple/pair
origin: 百词斩爱阅读
---

pair：强调合二为一，通常是指彼此不能分开的两件东西构成一个物品。比如：

- a pair of shoes
- a pair of earrings
- a pair of jeans

couple：强调两个相似的个体，特别指夫妻；或者相似的两三个。比如：

- a newly married couple
- a couple of weeks
- a couple of police officers
