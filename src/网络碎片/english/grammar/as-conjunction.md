---
title: as 连词用法
origin: 百词斩爱阅读
---

as 在英语中非常重要，词性也很丰富。

一、预备知识

1. 连词（conjunction）是干嘛的？——连接句子。

2. 如何识别连词？

- 直接背，因为数量不多：when/because/but/since 等。
- 连词后面会出现谓语动词。比如：as a teacher（as 是介词）, do as I told you.（as 是连词）

二、as 作连词的三种含义

1. 表方式：按照。

Do as I told you.

2. 表原因：由于、既然。

As everyone already knows each other, there's no need of introductions.（前后因果）

3. 表时间：当。

As she walked to the door, Sally thanked the couple for a lovely dinner.
