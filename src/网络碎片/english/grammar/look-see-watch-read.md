---
title: 表示“看”的单词
origin: 百词斩爱阅读
---

1. look：通常表示看这个动作。比如：

Look! He is over there.

2. see: 通常强调看到的内容和结果。比如：

How many words can you see in the sentence?

3. watch：通常用于专注、仔细地观看（电视、比赛等），强调看的过程。比如：

- watch a movie
- watch TV
- watch a match

4. read：读。比如：

- read books
- read newspapers
- read novel
