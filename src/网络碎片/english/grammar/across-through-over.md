---
title: “通过”
origin: 百词斩爱阅读
---

1. across：在物体表面进行，从一边移动到另一边。比如：

- across the street
- across the road
- access the border line（越过边境线）

2. through：从物体内部穿过，多半是三维空间。比如：

- through the door
- through a side entrance（从侧门穿过）
- through the jungle

3. over：从物体上面越过，不与物体接触。比如：

- over a broken piece of wood（跨过一块破木头）
- over the city（从城市上空越过）
- over a desert（越过沙漠）
