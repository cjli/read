---
title: nothing burger
origin: Louella Parsons, June 1, 1953.
---

A “nothing burger” (or “nothingburger) is a person who is a non-entity, or an idea that is without merit, or anything that’s a lot of nothing.

A “nothing burger” is a burger (such as a hamburger) without the burger.

> Something lame, dead-end, a dud, insignificant; especially something with high expectations that turns out to be average, pathetic, or overhyped.

