---
title: 10x engineer
origin: https://kateheddleston.com/blog/becoming-a-10x-developer
---

A 10x engineer isn’t someone who is 10x better than those around them, but someone who makes those around them 10x better.

> Ability to Win = Productivity = Σ(talent) * teamwork

