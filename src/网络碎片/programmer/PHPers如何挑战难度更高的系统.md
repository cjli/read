---
title: PHPers 如何挑战难度更高的系统
origin: http://blog.jobbole.com/26519/
---

一、高性能系统的挑战在哪里?

- 如何选择 WEB 服务器? 要不要使用 fast-cgi 模式。

- 要不要使用反向代理服务? 选择全内存缓存还是硬盘缓存?

- 是否需要负载均衡? 是基于应用层，还是网络层? 如何保证高可靠性?

- 你的 PHP 代码性能如何，使用优化工具后怎么样? 性能瓶颈在那里? 是否需要写成 C 的扩展?

- 用户访问有什么特点，是读多还是写多? 是否需要读写分离?

- 数据如何存储? 写入速度和读出速度如何? 数据增涨访问速读如何变化?

- 如何使用缓存? 怎么样考虑失效? 数据的一致性怎么保证?

二、高复杂性系统的挑战在哪里?

- 能否识别业务所对应的领域? 是一个还是多个?

- 能否合理对业务进行抽象，在业务规则变化能以很小的代价实现?

- 数据的一致性、安全性可否保证?

- 是否撑握了面向对象的分析和设计的方法？

以上问题如果你都能肯定的回答，我想在技术上你基本已经可能成为架构师了。

三、怎么样提高，突破瓶颈？

1. 分析你所使用的技术其原理和背后运行的机制，这样可以提高你的技术判断力，提高你技术方案选择的正确性;

2. 学习大学期间重要的知识, 操作系统原理，数据结构和算法。知道你以前学习都是为了考试，但现在你需要为自己学习，让自己知其所以然。

3. 重新开始学习C语言，虽然你在大学已经学过。这不仅是因为你可能需要写PHP扩展，而且还因为，在做C的应用中，有一个时刻关心性能、内存控制、变量生命周期、数据结构和算法的环境。

4. 学习面向对象的分析与设计，它是解决复杂问题的有效的方法。学习抽象，它是解决复杂问题的唯一之道。

四、系统学习 PHP 技术栈

0. 数学基础+计算机系统原理+数据结构+算法(大多数 PHPers 这个比较弱) ，需要不断重复的学习使用。

> 计算机科学本质上讲是数学的一个学科。好的数学家中间会产出优秀的程序员。不要让你的数学能力丧失殆尽。
> 逻辑学、离散数学、微积分、概率论、统计学、抽象代数、数论、范畴论、偏序理论这些数学知识尽量多练习，多熟悉下。

1. PHP 基础入门（语法，常用函数和扩展）

2. 面向对象的 PHP（书籍：《深入PHP，面向对象、模式与实践》）

其次是设计模式，尤其复杂的业务需求设计模式非常有帮助。

3. 网站软件架构设计（设计模式、框架等）

4. 网站物理层次架构设计（分布式计算、存储、负载均衡、高可用性等）

五、其他

- 积累：把常用的一些库(用过的，自己写的)都收集起来，在用到时拿出来用即可，非常方便。如：分页，图片处理，上传，下载，EMAIL 等等这些常用到的。

- 多方位动手：不光要写代码，把代码片段分析放到博客，也是进步提升的一个重要的环节，加深记忆不错的方法。
