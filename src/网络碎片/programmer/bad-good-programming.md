---
title: Bad programming vs Good programming
origin: 《How to Design Programs》，Matthias Felleisen
---

Bad programming is easy. Even dummies can learn it in 21 days.

Good programming requires thought, but everyone can do it and everyone can experience the extreme satisfaction that comes with it.
