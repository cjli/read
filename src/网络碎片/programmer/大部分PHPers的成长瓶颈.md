---
title: 大部分 PHPers 的成长瓶颈 
origin: http://blog.jobbole.com/26519/
---

1. 对 PHP 的掌握不精：很多 PHP 手册都没有看完，库除外。

2. 知识面比较窄：面对需求，除开使用 PHP 和 MySQL ，不知道其它的解决办法。

3. PHP 代码以过程为主：认为面向对象的实现太绕，看不懂。

这些 PHPer 在遇到需要高性能，处理高并发，大量数据的项目或业务逻辑比较复杂(系统需要解决多领域业务的问题)时，缺少思路。不能分析问题的本质，技术判断力比较差，对于问题较快能找出临时的解决办法，但常常在不断临时性的解决办法中，系统和自己一步步走向崩溃。
