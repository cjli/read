---
title: DNS 解析类型
origin: https://help.aliyun.com/knowledge_detail/29725.html
---

## A 记录

将域名指向 IP 地址。

## CNAME 记录

当需要将域名指向另一个域名，再由另一个域名提供 IP 地址，就需要添加 CNAME 记录，最常用到 CNAME 的场景包括做 CDN、企业邮箱、全局流量管理等。

## MX 记录

MX全称为mail exchanger，用于电子邮件系统发邮件时根据收信人的地址后缀来定位邮件服务器。

设置邮箱时，让邮箱能收到邮件，就需要添加 MX 记录。

例如，当有人发邮件给“vincen@example.com”时，系统将对“example.com”进行DNS中的MX记录解析。如果MX记录存在，系统就根据MX记录的优先级，将邮件转发到与该MX相应的邮件服务器上。

## AAAA 记录

当预期是实现访问者通过 IPv6 地址访问网站，可以使用 AAAA 记录实现。

## TXT 记录

如果希望对域名进行标识和说明，可以使用 TXT 记录， TXT 记录多用来做 SPF 记录（反垃圾邮件）。

## URL显性/隐性转发

将一个域名指向另外一个已经存在的站点时，需要添加 URL 记录。

示例：以 http://dns-example.com 跳转到 http://www.aliyun.com:80/ 为例。

### URL 隐性转发

用的是iframe框架技术，非重定向技术。

举例：浏览器地址栏输入http://dns-example.com 回车，打开网站内容是目标地址http://www.aliyun.com:80/ 的网站内容，但地址栏显示当前地http://dns-example.com。

### URL 显性转发

支持URL转发301永久重定向、302暂时性定向。

举例：浏览器地址栏输入http://dnswork.top 回车，打开网站内容是目标地址http://www.aliyun.com:80/ 的网站内容，且地址栏显示目标地址http://www.aliyun.com:80/

### 注意

- URL转发时记录值不能为IP地址
- URL转发不支持泛解析设置
- URL转发的目标域名不支持中文域名
- URL转发前域名支持HTTP，不支持HTTPS，转发后的目标地址支持HTTP、HTTPS

## NS 记录

如果需要把子域名交给其他 DNS 服务商解析，就需要添加 NS 记录。

示例：域名 dns-example.com 使用阿里云解析，将子域名www.dns-example.com 的解析管理权从阿里云解析授权给腾讯云解析做管理。

## SRV 记录

SRV 记录用来标识某台服务器使用了某个服务，常见于微软系统的目录管理。

「主机记录」格式：服务的名字.协议的类型。例如：`_sip._tcp`

「记录值」格式为 优先级 权重 端口 目标地址 ，每项中间需以空格分隔。例如：`0 5 5060 sipserver.example.com`

## CAA 记录

CAA(Certificate Authority Authorization)，即证书颁发机构授权。是一项新的可以添加到DNS记录中的额外字段,通过DNS机制创建CAA资源记录，可以限定域名颁发的证书和CA（证书颁发机构）之间的联系。未经授权的第三方尝试通过其他CA注册获取用于该域名的SSL/TLS证书将被拒绝。

域名设置 CAA 记录，使网站所有者，可授权指定CA机构为自己的域名颁发证书，以防止HTTPS证书错误签发，从而提高网站安全性。

CAA记录的格式为：`[flag] [tag] [value]`，是由一个标志字节的 `[flag]` 和一个被称为属性的 `[tag]-[value]`（标签-值）对组成。您可以将多个CAA字段添加到域名的DNS记录中。

### flag

0-255之间的无符号整数，用于标志认证机构。通常情况下填0，表示如果颁发证书机构无法识别本条信息，就忽略。

### tag

支持 issue、issuewild 和 iodef。

issue：CA授权单个证书颁发机构发布的 任何类型 域名证书。
issuewild：CA授权单个证书颁发机构发布主机名的 通配符 证书。
iodef：CA可以将违规的颁发记录URL发送给某个电子邮箱。

### value

CA 的域名或用于违规通知的电子邮箱。