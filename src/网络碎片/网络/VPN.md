---
title: VPN
---

Virtual Private Networks，虚拟专用网络。

VPN 这种网络技术可以将不同地点的单个网络联结成一个扩展的网络。换句话说，VPN 的目标是试图终结“地理位置的束缚”。