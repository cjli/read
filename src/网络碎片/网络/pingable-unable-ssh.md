---
title: VPS 能 PING 通 SSH 连不上？
origin: @qiushuiyibing
---

能 Ping 通只能代表 VPS 的 ICMP 协议是开启的，且该 IP 处于正常路由状态。

SSH 是具体的服务，连不上的原因有很多，比如服务没有启动、防火墙没有放行端口、TCP 通信因为某些原因被阻断等等。
