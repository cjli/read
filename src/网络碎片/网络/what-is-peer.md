---
title: What is network peer?
origin: <Computer Network>, Tanebaum
---

A five-layer network is illustrated in Fig. 1-13.

The entities comprising the corresponding layers on different machines are called peers.

The peers may be software processes, hardware devices, or even human beings.

In other words, it is the peers that communicate by using the protocol to talk to each other.
