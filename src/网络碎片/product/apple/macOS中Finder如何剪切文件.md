---
title: macOS 中 Finder 如何剪切文件？
origin: https://sspai.com/post/28389 
---

配合 Option 键（系统默认）。

第一步：在你要剪切的项目上右键单击，选择「复制」选项。

第二步：转到你需要粘贴的目录，右键单击，此时按住 Option 键，你会发现菜单中的「粘贴到此处」项变成了「移动到此处」。单击之来移动项目。

快捷键：你只需选中目标文件，然后使用 Command+C 复制，然后用 Command +Option+V 将其移动到目标目录。