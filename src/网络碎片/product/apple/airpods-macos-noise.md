---
title: AirPods 连接 macOS 有杂音？
tags: macOS, 蓝牙耳机, AirPods
origin: https://www.v2ex.com/t/363953 
---

蓝牙耳机在 macOS 上面音质差是苹果已知问题，是因为 Mac 默认同时打开耳机和麦克风引起的。

或者说 Xcode 的 Simulator 有冲突，打开 Simulator 的时候会自动切换到蓝牙耳机的麦克风，导致悲剧。有些时候在不开模拟器的时候也会出现这种情况。

在系统设置 -> 声音 -> 输入里，要是声音输入设备是 AirPods 的话就是这个问题了。

解决办法是：

蓝牙连接 Mac 后，进入麦克风设置，把麦克风调整为内置，音质就恢复了。

