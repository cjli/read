---
title: 更换 AppleID 为 iCloud 邮箱
origin: https://support.apple.com/zh-cn/HT202667#appleaddress
---

首先，你的 Apple Id 是以非 @icloud.com/@me.com/@mac.com 结尾的第三方Email地址，而你要把这个 Apple ID 改成以 @icloud 邮箱结尾，步骤如下：

- 前往 appleid.apple.com 并登录。

- 在“帐户”部分中，点按“编辑”。

- 点按 Apple ID 下方的“编辑 Apple ID”。

> 如果您尝试将自己的 Apple ID 更改为过去 30 天内创建的 @icloud.com 电子邮件地址，系统可能会提示您稍后再试。

- 输入您想要使用的 Apple ID（以 @icloud.com 结尾）。

- 点按“继续”。

将 Apple ID 更改为 @icloud.com、@me.com 或 @mac.com 帐户后，您无法再将它重新更改为第三方电子邮件帐户。您先前那个以第三方电子邮件地址结尾的 Apple ID 会变为您 Apple ID 帐户的其他电子邮件地址（可删除）。