---
title: disk rdisk difference
origin: https://superuser.com/questions/631592/why-is-dev-rdisk-about-20-times-faster-than-dev-disk-in-mac-os-x 
---

rdisk stands for raw disk.

In OS X each disk may have two path references in /dev:

- /dev/disk# is a buffered device, which means any data being sent undergoes extra processing
- /dev/rdisk# is a raw path, which is much faster, and perfectly OK when using the `dd` program.