---
title: nice 值
origin: ruanyf 
---

Unix系统的每个进程，都有一个nice值。这个值越大，优先级越低。然后，还有一个nice命令，每运行一次，指定进程的nice值＋10。它的意思就是做人要nice，把更多的CPU时间留给别人。nice值越高，你留给自己的份额就越少。