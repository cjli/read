---
title: BusyBox: The Swiss Army Knife of Embedded Linux
origin: https://zh.wikipedia.org/wiki/BusyBox
---

BusyBox是一个遵循GPL(v2)协议、以自由软件形式发行的应用程序。

Busybox在单一的可执行文件中提供了精简的Unix工具集，可运行于多款POSIX环境的操作系统，例如Linux（包括Android）、Hurd、FreeBSD等等。

由于BusyBox可执行文件的文件大小比较小、并通常使用Linux内核，这使得它非常适合使用于嵌入式系统。作者将BusyBox称为“嵌入式Linux的瑞士军刀”。
