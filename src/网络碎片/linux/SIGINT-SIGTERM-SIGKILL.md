---
title: SIGINT vs SIGTERM vs SIGKILL
---

三者都是结束/终止进程运行。

1. SIGINT SIGTERM区别

前者与字符 ctrl+c 关联，后者没有任何控制字符关联。

前者只能结束前台进程，后者则不是。

2. SIGTERM SIGKILL的区别

前者可以被阻塞、处理和忽略，但是后者不可以。KILL 命令的默认不带参数发送的信号就是SIGTERM，让程序友好地退出。

所以有的进程不能被结束时，用kill发送后者信号，即可。即：kill -9 进程号。