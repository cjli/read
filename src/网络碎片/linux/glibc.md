---
title: glibc 被卸载后会怎样？
origin: https://zhuanlan.zhihu.com/p/20062978
---

glibc被卸载，负责加载所有.so的ld.so也就没了，因此运行几乎所有外部命令时都会得到一句『找不到ld-linux-x-y-z.so.2』的出错提示。比如ls，比如cp，以及所有动态链接的命令。
