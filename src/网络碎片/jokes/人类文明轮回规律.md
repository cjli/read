---
title: 人类文明轮回规律
---

Hard times create strong men.

Strong men create good times.

Good times create week men.

Weak men create hard times.