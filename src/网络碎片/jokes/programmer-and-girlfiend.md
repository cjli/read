---
title: Programmer and girlfiend
---

DELETE FROM hope
JOIN person ON hope.person = person.id
WHERE hope.target = 'girlfiend'
AND person.occupation = 'programmer'
;