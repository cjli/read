---
title: Men and Times
---

Hard times create strong men.

Strong men create good times.

Good times create weak men.

Weak men create hard times.
