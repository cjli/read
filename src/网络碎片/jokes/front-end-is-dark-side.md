---
title: Front-end is the path to the dark side
---

Front-end leads to HTML, HTML leads to scripting, scripting leads to suffering.