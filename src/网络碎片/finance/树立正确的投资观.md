---
title: 树立正确的投资观
---

预测市场涨跌，短期追涨杀跌始终不是一整套的投资观；投资不一定每把投资操作都要赚取高额收益，即使骨灰级投资大师也难以做到这种程度；我们应该寻找到自己的适合投资方式，因为每个人的性格、资产以及风险承受能力都有所不同，短平快的投资观始终会让你失去耐心并承受亏钱的风险。

你的投资观是如何定性的关系到你的操作规则与纪律，好的投资观可以从理性上战胜心魔，而贪婪的投资观会让心魔战胜理性。

每一位在市场存活数年的投资者都无一例外的自建一套适合自己的投资观，坐拥数十万的基金理财投资大V银行螺丝钉，通过跟踪指数基金市盈率估值PE来判断基金进场、出场时机，通过指数市盈率估值实现基金的低买高卖，以成本低廉的指数基金作为工具赚取市场的财富.

银行螺丝钉一通过“闲钱、长期、PE低估、指数、耐心、纪律”，六大要素构成的一整套适于自己的投资观，虽然平均年化收益率比不上玩股票但也有保持在年均化12％以上。

基金定投是长期投资的中庸状态, 这种投资观非常的简单，简单到谁都可以复制，采取的操作手法是利用市场波动进行长期的定投，实现基金收益的策略；需选择周期性波
动、远期大概率上涨的基金品种，以指数增强基金为主，配置有行业型基金；实行增量资金+逆向投资，分阶段达到既定目标后，再转型稳健配置。

实际上, 要通过长时间的探索与分析才能找出适于自己的投资观，投资实践与反省永远都是无法作弊的一个过程，你想要什么样的结果，就需要做什么样的努力。

许多基民喜欢用“炒”字形容买卖基金。“炒”自然就是投机，从字面上看就明白不是投资了。

投机是瞄准一个“机会”，希望大赚一笔，一劳永逸，而投资是要你扎扎实实、一点一滴地走向你的最终梦想。

投机实际上更多的是收获风险和灾难，而投资则是一种理性的产物。从“炒”转向“投”也是一种投资观的必要性改变。

简单的投资观并不是自相矛盾而是信手拈来，这才是简明扼要的投资之道。投资观就是把你的投资知识与技巧不断的压榨缩小成精华. 有句老话说：“书是越看越薄的”, 我们在看教科书，第一遍是需要记忆的知识点很多，所消耗的时间是最长的，书也是最厚的；但经过你数十遍的翻看，书自然而然的就变薄了，而翻看的速度就快了。
