---
title: 金钱不能使你富有
origin: https://baike.baidu.com/item/%E8%80%81%E9%BC%A0%E8%B5%9B%E8%B7%91/7425375
---

现金流游戏最大的功能在于让你看清金钱及金钱的规律，当游戏开始时，我们都得到了不同数量的“纸币”，用生活必须支出后剩下的钱参与金钱的游戏。

这时候你会发现钱真的是“纸”，它不能让你富有。要想逃出“老鼠赛跑”，靠你手上的钱哪怕钱再多也不能使你进入富裕状态。

当你在“老鼠赛跑”的时候，并非你就挣不到钱，你甚至可以挣很多钱，但钱不能使你富有，因为你没有资产，当然也没有资产给你带来的金钱，你的钱全部来自于你的工资，当你不工作的时候你就变得一无所有。

而真正的富裕是动态的，是现金流源源不断地流向你的口袋。要达到此目的，你必须从小规模的投资开始不断地创造你的资产，再用资产带来的钱创建更多的资产。

当你的钱越来越多的时候，你不一定是富裕的，因为你的钱有可能是来自于你的加薪。

而当你的资产越来越多的时候你肯定是富有的，因为你的钱来自于属于你的资产，无论这些资产是房地产、企业、还是“纸上”的资产如：股票、手稿、存款等等。

