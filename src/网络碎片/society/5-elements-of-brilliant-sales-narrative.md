---
title: 最有效的五步推销法
origin: https://medium.com/the-mission/the-greatest-sales-deck-ive-ever-seen-4f4ef3391ba0 
---

1. Name a Big, Relevant Change in the World

> 提出一个当今世界的变化趋势

2. Show There’ll Be Winners and Losers

> 提出会有赢家和输家

3. Tease the Promised Land

> 描绘美好的未来

4. Introduce Features as “Magic Gifts” for Overcoming Obstacles to the Promised Land

> 介绍克服困难、到达未来所需要的那个关键步骤

5. Present Evidence that You Can Make the Story Come True

> 提出证据，你能让这一切变成现实
